import React from 'react';
import ReactDOM from 'react-dom'; //we use it because we're not builting a native application ! 
import './index.css';
import App from './app/layout/App';
import * as serviceWorker from './serviceWorker';
import 'semantic-ui-css/semantic.min.css';

const rootEl = document.getElementById('root');
let render = () => {
    ReactDOM.render(<App/> , rootEl)
}
if(module.hot){
    module.hot.accept('./app/layout/App',()=>{
        setTimeout(render)
    })
}
render();

serviceWorker.register();
 